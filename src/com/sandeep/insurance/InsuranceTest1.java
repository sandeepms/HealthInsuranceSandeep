package com.sandeep.insurance;

import static org.junit.Assert.*;

import org.junit.Test;

public class InsuranceTest1 {
	   
	   @Test
	   public void testInsurance() {
		   Insurance obj = new Insurance();
		   Patient user=new Patient();
			user.setName("Norman Gomes");
			user.setGender("male");
			user.setAge(32);
			Health health=new Health();
			health.setBloodPressure("Yes");
			health.setBloodSugar("No");
			health.setHypertension("No");
			health.setOverweight("Yes");
			user.setCurrentHealth(health);
			Habits habit=new Habits();
			
			habit.setSmoking("Yes");
			habit.setAlcohol("Yes");
			habit.setDailyExercise("Yes");
			habit.setDrugs("No");
			user.setHabits(habit);
			
			double premium=obj.generateHealthInsurance(user);
			System.out.println("Health Insurance Premium for Mr. Gomes: Rs. "+ premium);
			
	   }

	   

}
