package com.sandeep.insurance;

public class Insurance {


		public static void main(String[] args) {
			getPatientdetails();
		}
		public static double getPatientdetails(){
			
			//passing the patient details as given  given
			Patient user=new Patient();
			user.setName("Norman Gomes");
			user.setGender("male");
			user.setAge(34);
			Health health=new Health();
			health.setBloodPressure("No");
			health.setBloodSugar("No");
			health.setHypertension("No");
			health.setOverweight("Yes");
			user.setCurrentHealth(health);
			Habits habit=new Habits();
			
			habit.setSmoking("No");
			habit.setAlcohol("Yes");
			habit.setDailyExercise("Yes");
			habit.setDrugs("No");
			user.setHabits(habit);
			
			double premium=generateHealthInsurance(user);
			System.out.println("Health Insurance Premium for Mr. Gomes: Rs. "+ premium);
			return premium;
		}
		
		public static double generateHealthInsurance(Patient userDetail){
			 @SuppressWarnings("unused")
			String  name=userDetail.getName();
			 int age=userDetail.getAge();
			 double basePremium=5000;
			 double premium =basePremium;
			if(age<18){
				premium=basePremium;
			}
			else if((age >=18 && age<=25) || (age >=25 && age<=30) ||(age >=30 && age<=35) ||(age >=35 && age<=40)){
				
				premium = premium + (0.1 * basePremium);
			}
			else if(age>=40){
				premium= premium + (0.2 * basePremium);
				
			}
			Health health=userDetail.getCurrentHealth();
			String hypertension=health.getHypertension();
			String bloodPressure=health.getBloodPressure();
			String bloodSugar=health.getBloodSugar();
			String overweight=health.getOverweight();
			
			if(hypertension.equals("Yes")){
				premium=premium + (0.01*premium);
			}
			if(bloodPressure.equals("Yes")){
				premium=premium + (0.01*premium);
			}
			if(bloodSugar.equals("Yes")){
				premium=premium + (0.01*premium);
			}
			if(overweight.equals("Yes")){
				premium=premium + (0.01*premium);
			}
			Habits habit=userDetail.getHabits();
			 String smoking=habit.getSmoking();
			 String alcohol=habit.getAlcohol();
			 String dailyExercise=habit.getDailyExercise();
			 String drugs=habit.getDrugs();
			 if(smoking.equals("Yes")){
					premium=premium + (0.03*premium);
				}
			 
			 if(alcohol.equals("Yes")){
					premium=premium +(0.03*premium);
				}
			 
			 if(dailyExercise.equals("Yes")){
					premium=premium - (0.03*premium);
				}
			 
			 if(drugs.equals("Yes")){
					premium=premium +(0.03*premium);
				}
			 
			 		
			
			String gender=userDetail.getGender();
			
			if(gender=="Male"){
				premium=premium + (0.02 * premium);
				
						}
			
			return premium;
		}

	}



