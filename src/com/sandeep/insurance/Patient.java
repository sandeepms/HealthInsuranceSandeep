package com.sandeep.insurance;

public class Patient {

	private String name;
	private String gender;
	private int age;
	private Health currentHealth;
	private Habits habits;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Health getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(Health currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
	

}
